" Encoding
set encoding=utf-8
scriptencoding utf-8

" Lazy redraw (faster macros)
set lazyredraw

" Leader
let g:mapleader = ' '
let g:maplocalleader = '\\'

" Reload .vim.init
nnoremap <silent> <F5> :source $MYVIMRC<CR>

" Persistent undo
set undofile
set undodir=$HOME/.local/share/nvim/undo
set undolevels=1000
set undoreload=10000

" Line length indicator
set colorcolumn=100

" Line numbering
set number
set relativenumber

" Leave 5 line ahead while scrolling
set scrolloff=5

" Search case
set ignorecase
set smartcase

" Tabs as 4 spaces
set tabstop=4
set shiftwidth=4
set expandtab

" Quick movement
noremap! <A-k> <up>
noremap! <A-j> <down>
noremap! <A-h> <left>
noremap! <A-l> <right>

" Put semicolon at the end of the line
noremap <leader>' m`$a;<ESC>``
noremap! <C-'> <ESC>m`$a;<ESC>``a

" Display tabs and tailing spaces
set listchars=tab:▸\ ,trail:·
set list

" Use system clipboard
set clipboard=unnamed

" Use mouse
set mouse=a

" Cursor shape
set guicursor=n-o:hor50,i-c-ci:ver25,v-r-cr:block
            \,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor
            \,sm:hor50-blinkwait175-blinkoff150-blinkon175

" Spell checking
augroup spell_aug
    autocmd!
    autocmd FileType markdown,md,html,htm,text setlocal spell spelllang=en,pl
    autocmd TermOpen * set nospell
augroup END

" Folding
set foldmethod=indent
set nofoldenable

" Line wrapping
set nowrap 
augroup wrap_aug
    autocmd!
    autocmd FileType markdown,md,html,htm,text setlocal wrap
    autocmd FileType markdown,md,html,htm,text setlocal linebreak
    autocmd FileType markdown,md,html,htm,text setlocal breakindent
augroup END

" Quick $MYVIMRC split
nnoremap <F6> :vsplit $MYVIMRC<cr>

" Open new horizontal splits below active window
set splitbelow

" Quick save
nnoremap <C-s> :w<CR>
inoremap <C-s> <ESC>:w<CR>

" Quick buffer operations
nnoremap <silent> <leader>q :q<CR>
function! NextBuffer()
    bnext!
    if !empty(matchstr(nvim_buf_get_name(0), 'term://\.//.*'))
        bnext!
    endif
endfunction

function! PrevBuffer()
    bprev!
    if !empty(matchstr(nvim_buf_get_name(0), 'term://\.//.*'))
        bprev!
    endif
endfunction

function! DelBuffer()
    if !empty(matchstr(nvim_buf_get_name(0), 'term://\.//.*'))
        bdelete!
    else
        bdelete
    endif
    if !empty(matchstr(nvim_buf_get_name(0), 'term://\.//.*'))
        bprev!
    endif
endfunction

nnoremap <silent> <A-l> :call NextBuffer()<CR>
nnoremap <silent> <A-h> :call PrevBuffer()<CR>
nnoremap <silent> <C-q> :call DelBuffer()<CR>
tnoremap <silent> <C-q> <C-\><C-n>:bd!<CR>

" Exiting terminal via <ESC>
tnoremap <ESC> <C-\><C-n>

" Quick quit
nnoremap <silent> <leader>q :q<CR>

" Delete without copying
vnoremap <C-q> <ESC>
noremap <leader>d "_d
noremap <leader>D "_D
noremap <leader>s "_s
noremap <leader>S "_S
noremap <leader>c "_c
noremap <leader>C "_C
noremap x "_x
noremap X "_X

" Delete with copying
noremap <leader>x x
noremap <leader>X X

" Insert delete
noremap! <C-x> <del>

" Capitalize a word
noremap!<C-u> <ESC>viWUEa
noremap U viWUE

" Use Y for copying to end of the line
noremap Y y$

" Clear search
nnoremap <silent> <leader>a :nohlsearch<CR>

" Enable truecolor
set termguicolors

" Highlight current line
set cursorline
highlight CursorLine ctermbg=235
highlight CursorLineNR ctermbg=235
highlight CursorLineNR ctermfg=248

" Better vertical split separator
set fillchars+=vert:∣

" Plugins
call plug#begin('~/.local/share/nvim/plugged')
nnoremap <silent> <F4> :PlugInstall<CR>

" Repeat plugin mappings with '.'
Plug 'tpope/vim-repeat'

" Quotes, parenthesis, brackets, etc autocompletion
Plug 'Raimondi/delimitMate'

" HTML toolkit
Plug 'mattn/emmet-vim'
let g:user_emmet_install_global=0
augroup emmet_aug
    autocmd!
    autocmd FileType html,css,hbs,javascript EmmetInstall
    autocmd FileType html,css,hbs,javascript nmap <tab> <C-y>,
    autocmd FileType html,css,hbs,javascript imap <localleader>e <ESC><C-y>,
    autocmd FileType html,css,hbs,javascript vmap <localleader>e <C-y>,
augroup END

" Commenting with gc, gcc (not the compiler)
Plug 'tpope/vim-commentary'
augroup commentary_aug
    autocmd!
    autocmd FileType c,cpp setlocal commentstring=//\ %s
augroup END

" Easy finding via <leader>[fFtTbwe]
Plug 'easymotion/vim-easymotion'
map <leader> <Plug>(easymotion-prefix)

" Surroundings edition with [cdy]s
Plug 'tpope/vim-surround'
let g:NERDCompactSexyComs=1
let g:NERDCommentEmptyLines=1

" Allow [fFtT] to work accross multiple lines
Plug 'dahu/vim-fanfingtastic'

" Textobject user definition
Plug 'kana/vim-textobj-user'

" Textobject spanning entire buffer (ae)
Plug 'kana/vim-textobj-entire'

" Textobject for comments
Plug 'glts/vim-textobj-comment'

" Language client
Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }

" Required for operations modifying multiple buffers like rename.
set hidden
set signcolumn=yes

let g:LanguageClient_serverCommands = {
    \ 'rust': ['/home/zbrojny120/.cargo/bin/rustup', 'run', 'stable', 'rls'],
    \ 'typescript': ['javascript-typescript-stdio'],
    \ 'javascript': ['javascript-typescript-stdio'],
    \ 'python': ['pyls'],
    \ 'c': ['~/Repositories/ccls/Release/ccls', '--log-file=/tmp/cc.log'],
    \ 'cpp': ['~/Repositories/ccls/Release/ccls', '--log-file=/tmp/cc.log'],
    \ 'cuda': ['~/Repositories/ccls/Release/ccls', '--log-file=/tmp/cc.log'],
    \ 'objc': ['~/Repositories/ccls/Release/ccls', '--log-file=/tmp/cc.log'],
    \ 'sh': ['bash-language-server', 'start']
    \ }

let g:LanguageClient_useVirtualText = 0

nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>

" Multi-entry selection UI.
Plug 'junegunn/fzf'

" Autocompletion
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
let g:deoplete#enable_at_startup = 1

" Ctags interface
Plug 'majutsushi/tagbar'
nnoremap <silent> <leader>u :TagbarToggle<CR>
let g:tagbar_ctags_bin='/usr/bin/ctags'

" Engine for snippets activated with <Tab>
Plug 'SirVer/ultisnips'
let g:UltiSnipsJumpForwardTrigger='<c-b>'
let g:UltiSnipsJumpBackwardTrigger='<c-z>'

" Default snippet collection
Plug 'honza/vim-snippets'

" Find files with <C-p>
Plug 'ctrlpvim/ctrlp.vim'

" Git wrapper
Plug 'tpope/vim-fugitive'

" Vertical line showing indentation
Plug 'Yggdroot/indentLine'
let g:indentLine_char = '│'
let g:indentLine_color_term = 239

" Ack support
Plug 'mileszs/ack.vim'

" Better C++ support
Plug 'octol/vim-cpp-enhanced-highlight'

" GLSL support
Plug 'tikhomirov/vim-glsl'

" Handlebars support
Plug 'mustache/vim-mustache-handlebars'

" Better javascript support
Plug 'pangloss/vim-javascript'

" JSX support
Plug 'mxw/vim-jsx'

" TypeScript support
Plug 'HerringtonDarkholme/yats.vim'
Plug 'mhartington/nvim-typescript', {'do': './install.sh'}

augroup ts_aug
    autocmd!
    autocmd FileType typescript setfiletype typescript.jsx
augroup END

" HTML5 support
Plug 'othree/html5.vim'

" Pug support
Plug 'digitaltoad/vim-pug'

" Rust support
Plug 'rust-lang/rust.vim'

" Go support
Plug 'fatih/vim-go'

" Elixir support
Plug 'elixir-editors/vim-elixir'

" Ansible support
Plug 'pearofducks/ansible-vim'

" Pretty statusline
Plug 'vim-airline/vim-airline'
set noshowmode
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 1

" Airline themes
Plug 'vim-airline/vim-airline-themes'

" Colorschemes
Plug 'tyrannicaltoucan/vim-deep-space'
Plug 'w0ng/vim-hybrid'
Plug 'ciaranm/inkpot'
Plug 'dracula/vim'
Plug 'whatyouhide/vim-gotham'
Plug 'Badacadabra/vim-archery'
Plug 'drewtempelmeyer/palenight.vim'
Plug 'morhetz/gruvbox'
Plug 'romainl/Apprentice'
Plug 'NLKNguyen/papercolor-theme'
let g:airline_theme='gruvbox'
let g:palenight_terminal_italics=1

call plug#end()

" Colorscheme, has to be set after plug#end()
set background=dark
colorscheme gruvbox

" Line number color
highlight LineNr guifg=#808080
